#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Bool, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


_STATES = {
    'readonly': Bool(Eval('invoice_line')),
}
_DEPENDS = ['invoice_line']


class BillingLine(ModelSQL, ModelView):
    'Billing Line'
    _name = 'account.invoice.billing_line'
    _description = __doc__
    _rec_name = 'party'

    product = fields.Many2One('product.product', 'Product', required=True,
            on_change=['product'], states=_STATES, select=1, depends=_DEPENDS)
    quantity = fields.Float('Quantity', digits=(16, Eval('unit_digits', 2)),
            required=True, states=_STATES, depends=_DEPENDS+['unit_digits'])
    party = fields.Many2One('party.party', 'Party', required=True,
            states=_STATES, select=1, depends=_DEPENDS)
    unit = fields.Many2One('product.uom', 'Unit', required=True, states=_STATES,
            depends=_DEPENDS+['product'], domain=[
                ('category', '=',
                    (Eval('product'), 'product.default_uom.category')),
            ],
            context={
                'category': (Eval('product'), 'product.default_uom.category'),
            })
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
            'get_unit_digits')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
            readonly=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=_STATES, select=2, depends=_DEPENDS)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('cancel', 'Canceled'),
            ('invoice', 'Invoiced')
            ], 'State', readonly=True, required=True, select=1)
    billing_date = fields.Date('Date', states=_STATES, required=True,
            depends=_DEPENDS, select=1)

    def __init__(self):
        super(BillingLine, self).__init__()
        self._error_messages.update({
            'delete_invoiced_lines': 'You cannot delete invoiced lines!',
            'missing_invoice_address': 'Can not find invoice address for '
                'party %s (%s). Please define one.',
            'no_fiscalyear_for_date':
                'Can not find a fiscal year for billing date %s',
            })

    def default_company(self):
        return Transaction().context.get('company', False)

    def default_state(self):
        return 'draft'

    def default_billing_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def on_change_product(self, vals):
        product_obj = Pool().get('product.product')
        res = {}
        if vals.get('product'):
            product = product_obj.browse(vals['product'])
            res['unit'] = product.default_uom.id
            res['unit.rec_name'] = product.default_uom.rec_name
            res['unit_digits'] = product.default_uom.digits
        return res

    def on_change_with_unit_digits(self, vals):
        uom_obj = Pool().get('product.uom')
        if vals.get('unit'):
            uom = uom_obj.browse(vals['unit'])
            return uom.digits
        return 2

    def get_unit_digits(self, ids, name):
        res = {}
        for line in self.browse(ids):
            if line.unit:
                res[line.id] = line.unit.digits
            else:
                res[line.id] = 2
        return res

    def delete(self, ids):
        billing_lines = self.browse(ids)
        for line in billing_lines:
            if line.invoice_line:
                self.raise_user_error('delete_invoiced_lines')
        return super(BillingLine, self).delete(ids)

    def trigger_reset_state_billing_line(self, invoice_line_ids, trigger_id):
        invoice_line_obj = Pool().get('account.invoice.line')
        records = invoice_line_obj.read(invoice_line_ids,
            fields_names=[('billing_lines')])
        billing_line_ids = []
        for record in records:
            billing_line_ids += record['billing_lines']
        self._reset_state_billing_line(billing_line_ids)

    def _reset_state_billing_line(self, billing_line_ids):
        self.write(billing_line_ids, {'state': 'draft'})

    def create_invoices(self, billing_line_ids=None):
        '''
        BillingLine.create_invoices(billing_line_ids=None)

        Create invoices from billing line ids. Billing lines
        with same company, party and year of billing_date are
        collected in one invoice.

        :param billing_line_ids: a list of billing_line ids
        :return: a list of created invoice ids
        '''
        if not billing_line_ids:
            return []

        invoice_ids = []
        for billing_line in self.browse(billing_line_ids):
            invoice_id, accounting_date = self._get_invoice_billing(billing_line, invoice_ids)
            if invoice_id not in invoice_ids:
                invoice_ids.append(invoice_id)
            self._create_invoice_lines(billing_line, invoice_id,
                    accounting_date)
        return invoice_ids

    def _get_invoice_billing(self, billing_line, invoice_ids):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        fiscalyear_obj = pool.get('account.fiscalyear')
        party_obj = pool.get('party.party')
        journal_obj = pool.get('account.journal')
        user_obj = pool.get('res.user')

        update_invoice = False
        fiscalyear = fiscalyear_obj.search([
            ('start_date', '<=', billing_line.billing_date),
            ('end_date', '>=', billing_line.billing_date),
            ])
        if not fiscalyear:
            user = user_obj.browse(Transaction().user)
            lang = user.language
            self.raise_user_error('no_fiscalyear_for_date', error_args=(
                billing_line.billing_date.strftime(str(lang.date))))
        for invoice in invoice_obj.browse(invoice_ids):
            invoice_id = invoice.id
            update_invoice = (
                invoice.company == billing_line.company
                and invoice.party == billing_line.party
                and invoice.accounting_date.year
                    == billing_line.billing_date.year)
            if update_invoice:
                invoice_values = {}
                accounting_date = invoice.accounting_date
                if billing_line.billing_date > invoice.accounting_date:
                    accounting_date = billing_line.billing_date
                    invoice_values['accounting_date'] = accounting_date
                self._update_invoice(invoice, invoice_values,
                    billing_line)
                break

        if not update_invoice:
            accounting_date = billing_line.billing_date
            party_address_id = party_obj.address_get(
                billing_line.party.id, type='invoice')
            if not party_address_id:
                self.raise_user_error('missing_invoice_address',
                    error_args=(
                        billing_line.party.rec_name,
                        billing_line.party.code,
                    ))
            journal_id, = journal_obj.search([('type', '=', 'revenue')])
            invoice_values = {
                'type': 'out_invoice',
                'company': billing_line.company.id,
                'party': billing_line.party.id,
                'currency': billing_line.company.currency.id,
                'account': billing_line.party.account_receivable.id,
                'payment_term': billing_line.party.payment_term.id,
                'invoice_address': party_address_id,
                'journal': journal_id,
                'accounting_date': accounting_date,
                }
            invoice_id = self._create_invoice(invoice_values,
                billing_line)
        return invoice_id, accounting_date

    def _update_invoice(self, invoice, invoice_values, billing_line):
        invoice_obj = Pool().get('account.invoice')
        with Transaction().set_user(0, set_context=True):
            res = invoice_obj.write(invoice.id, invoice_values)

        return res

    def _create_invoice(self, invoice_values, billing_line):
        '''
        BillingLine._create_invoice(invoice_values, billing_line,
            accounting_date)

        Method which creates an invoice using invoice_values dictionary,
        with account.invoice model field names as keys. Inherit this method
        to modify the invoice_values for the newly created invoice.
        :param invoice_values: a dictionary with account.invoice model field
            names as keys.
        :param billing_line: a billing line obj.

        :return: Id of the newly created invoice.
        ::
        '''
        invoice_obj = Pool().get('account.invoice')
        with Transaction().set_user(0, set_context=True):
            res = invoice_obj.create(invoice_values)
        return res

    def _create_invoice_lines(self, lines, invoice_id, accounting_date):
        pool = Pool()
        billing_line_obj = pool.get('account.invoice.billing_line')
        invoice_line_obj = pool.get('account.invoice.line')
        invoice_obj = pool.get('account.invoice')
        if not isinstance(lines, list):
            lines = [lines]

        for line in lines:
            vals = self._get_invoice_line_billing(line, invoice_id,
                accounting_date)
            with Transaction().set_user(0, set_context=True):
                invoice_line_id = invoice_line_obj.create(vals)
                invoice_obj.update_taxes([invoice_id])
            billing_line_obj.write(line.id, {
                'invoice_line': invoice_line_id,
                'state': 'invoice'
                })

    def _get_invoice_line_billing(self, line, invoice_id, accounting_date):
        taxes = self._get_taxes_from_line(line)
        res = {
            'invoice': invoice_id,
            'type': 'line',
            'quantity': line.quantity,
            'unit': line.unit.id,
            'product': line.product.id,
            'account': self._get_account_from_line(line),
            'unit_price': self._get_unit_price_from_line(line),
            'description': line.product.name,
            'taxes': [('add', taxes)],
            }
        return res

    def _get_taxes_from_line(self, line):
        tax_rule_obj = Pool().get('account.tax.rule')
        res = []
        pattern = self._get_tax_rule_pattern(line, {})
        for tax in line.product.customer_taxes_used:
            if line.party and line.party.customer_tax_rule:
                tax_ids = tax_rule_obj.apply(line.party.customer_tax_rule, tax,
                        pattern)
                if tax_ids:
                    res.extend(tax_ids)
                continue
            res.append(tax.id)
        if line.party and line.party.customer_tax_rule:
            tax_ids = tax_rule_obj.apply(line.party.customer_tax_rule, False,
                    pattern)
            if tax_ids:
                res.extend(tax_ids)
        return res

    def _get_account_from_line(self, line):
        return line.product.account_revenue_used.id

    def _get_unit_price_from_line(self, line):
        return line.product.list_price

    def _get_tax_rule_pattern(self, line, vals):
        '''
        Get tax rule pattern

        :param line: the BrowseRecord of the billing_line
        :param vals: a dictionary with value for the future invoice line
        :return: a dictionary to use as pattern for tax rule
        '''
        res = {}
        return res

BillingLine()


class CreateInvoicesAskTerm(ModelView):
    'Create Invoices Ask Term'
    _name = 'account.invoice.billing_line.create_invoices.ask_term'
    _description = __doc__
    party = fields.Many2One('party.party', 'Customer', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    payment_term = fields.Many2One(
        'account.invoice.payment_term', 'Payment Term', required=True)

CreateInvoicesAskTerm()


class CreateInvoices(Wizard):
    'Create Invoices'
    _name = 'account.invoice.billing_line.create_invoices'

    states = {

        'init': {
            'result': {
                'type': 'choice',
                'next_state': '_create_invoices',
                },
            },

        'ask_term': {
            'actions': ['_set_default_payment_term'],
            'result': {
                'type': 'form',
                'object': 'account.invoice.billing_line.create_invoices.ask_term',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('init', 'Continue', 'tryton-go-next', True),
                    ],
                },
            },

        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open_invoices',
                'state': 'end',
                },
            },
        }

    def __init__(self):
        super(CreateInvoices, self).__init__()
        self._error_messages.update({
            'no_lines': 'No valid selection!\n'
                    'Billing lines must be selected and in state "Draft" '
                    'to be proceeded.',
            })

    def _set_default_payment_term(self, data):
        billing_line_obj = Pool().get('account.invoice.billing_line')

        line_ids = billing_line_obj.search([('invoice_line', '=', False)])
        billing_lines = billing_line_obj.browse(line_ids)

        for line in billing_lines:
            if line.party.payment_term:
                continue
            if not line.party.payment_term:
                return {'party': line.party.id,'company': line.company.id}

        return {'party': line.party.id,'company': line.company.id}

    def _create_invoices(self, data):
        pool = Pool()
        billing_line_obj = pool.get('account.invoice.billing_line')
        party_obj = pool.get('party.party')
        company_obj = pool.get('company.company')
        invoice_obj = pool.get('account.invoice')

        form = data['form']

        if form.get('payment_term') and form.get('party') and \
                form.get('company'):
            with Transaction().set_context(company=form['company']):
                party_obj.write(form['party'], {
                        'payment_term': form['payment_term']
                        })

        billing_line_ids = billing_line_obj.search([
                ('state', '=', 'draft'),
                ('id', 'in', data['ids']),
                ], order=[('billing_date', 'ASC')])

        if not billing_line_ids:
            self.raise_user_error('no_lines')

        billing_lines = billing_line_obj.browse(billing_line_ids)

        for line in billing_lines:
            if not line.party.payment_term:
                return 'ask_term'

        billing_line_obj.create_invoices(billing_line_ids)

        return 'open'

    def _action_open_invoices(self, data):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        args = [('state', '=', 'draft')]
        invoice_ids = invoice_obj.search(args)

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_invoice_out_invoice_form'),
            ('module', '=', 'account_invoice'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['domain'] = str([
            ('id', 'in', invoice_ids),
            ])

        return res

CreateInvoices()
