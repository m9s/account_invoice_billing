#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing',
    'name_de_DE': 'Fakturierung Rechnungsstellung aus Abrechnungsdaten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Billing
    - Provides the possibility to create invoices directly from billing lines
      listed in a separate table.
''',
    'description_de_DE': '''Fakturierung Rechnungsstellung aus Abrechnungsdaten
    - Ermöglicht die Generierung von Rechnungen auf Basis von in einer
      Tabelle erfassten Abrechnungsdaten.
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
        'billing.xml',
        'invoice.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
